job "text-to-hades" {
  type = "service"

  group "app" {
    task "bot" {
      driver = "podman"

      config {
        image = "docker://registry.gitlab.com/arno500/text-to-hades:${CI_COMMIT_SHA}"
      }

      resources {
        memory_max = 4000
      }

      template {
        destination  = "secrets/.env"
        env          = true
        data         = <<EOH
{{ with nomadVar "nomad/jobs/text-to-hades/app/bot" }}
DISCORD_TOKEN="{{ .DISCORD_TOKEN }}"
DISCORD_CLIENT_ID="{{ .DISCORD_CLIENT_ID }}"
{{ end }}
MIMIC_URL="http://{{ env "NOMAD_ADDR_mimic" }}"
EOH
      }
    }

    network {
      port "mimic" { to = 59125 }
    }

    task "mimic" {
      driver = "podman"

      config {
        image = "docker://mycroftai/mimic3:latest"
        ports = ["mimic"]
        volumes = [
          "/mnt/b/volumes/mimic:/home/mimic3/.local/share/mycroft/mimic3:Z,noexec"
        ]
      }

      resources {
        memory_max = 2000
      }

      service {
        name = "mimic"
        port = "mimic"
        provider = "consul"
      }
    }
  }
}
