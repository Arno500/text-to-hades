// All configuration options: https://remotion.dev/docs/config
// Each option also is available as a CLI flag: https://remotion.dev/docs/cli
// ! The configuration file does only apply if you render via the CLI !

import {Config} from 'remotion';

Config.setChromiumOpenGlRenderer('angle');
Config.setCodec('vp9');
Config.setQuality(90);
Config.setCrf(35);
Config.setImageFormat('jpeg');
Config.setOverwriteOutput(true);
Config.setMaxTimelineTracks(100);
