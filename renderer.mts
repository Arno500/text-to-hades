import { bundle } from "@remotion/bundler";
import { getCompositions, renderMedia } from "@remotion/renderer";
import { randomUUID } from "crypto";
import path from "path";
import pThrottle from "p-throttle";
import { textToSpeech } from "./src/TextToSpeech/index.js";
import { cpus } from "os";

let bundleLocation: string

export const init = async () => {
    // You only have to do this once, you can reuse the bundle.
    const entry = "./src/index.ts";
    console.log("Creating a Webpack bundle of the video");
    bundleLocation = await bundle(path.resolve(entry), () => undefined, {
        // If you have a Webpack override, make sure to add it here
        webpackOverride: (config) => config,
        enableCaching: true
    });
};

const ttsLimit = pThrottle({
    interval: 1000,
    limit: 4,
});

export const render = async (messages: Array<{
    content: string,
    author: {
        id: string,
        name: string,
    },
    audioSrc?: string
}>) => {
    // The composition you want to render
    const compositionId = "Compositor";
    const authors = [...(new Set(messages.map((message) => message.author.id)))]
    // Parametrize the video by passing arbitrary props to your component.
    const inputProps = {
        messages,
        authors: authors.map((author) => ({ id: author, name: messages.find((message) => message.author.id === author)?.author.name ?? "" }))
    };

    console.log("Converting text to speech")
    const throttledTTS = ttsLimit(textToSpeech)
    await Promise.all(inputProps.messages.map(async (message) =>
        message.audioSrc = await throttledTTS(message.content, "fr_FR", authors.indexOf(message.author.id))
    ))
    // Extract all the compositions you have defined in your project
    // from the webpack bundle.
    console.log("Extracting compositions from the bundle")
    await init();
    const comps = await getCompositions(bundleLocation, {
        // You can pass custom input props that you can retrieve using getInputProps()
        // in the composition list. Use this if you want to dynamically set the duration or
        // dimensions of the video.
        inputProps,
        onBrowserLog: (info) => {
            console.log(`${info.type}: ${info.text}`);
            console.log(
                info.stackTrace
                    .map((stack) => {
                        return `  ${stack.url}:${stack.lineNumber}:${stack.columnNumber}`;
                    })
                    .join("\n")
            );
        }
    });

    // Select the composition you want to render.
    const composition = comps.find((c) => c.id === compositionId);

    // Ensure the composition exists
    if (!composition) {
        throw new Error(`No composition with the ID ${compositionId} found.`);
    }

    const outputLocation = `out/${randomUUID()}.mp4`;
    console.log("Attempting to render:", outputLocation);
    await renderMedia({
        composition,
        serveUrl: bundleLocation,
        codec: "h264",
        crf: 25,
        imageFormat: "jpeg",
        quality: 90,
        audioCodec: "aac",
        audioBitrate: "96k",
        outputLocation,
        inputProps,
        concurrency: cpus().length,
        scale: 0.7
    });
    console.log("Render done!");
    return outputLocation;
}

// await init();
// await render([{
//     content: "Coucou, tu veux voir ma bite ?",
//     author: {
//         id: "1",
//         name: "ArDu",
//     }
// }, {
//     content: "Oh oui, j'en ai tellement envie",
//     author: {
//         id: "2",
//         name: "Kevin",
//     }
// }]);

// await render([{
//     author: { id: '215382157500219392', name: 'ArDu' },
//     content: 'Turbine de foutre salopée et trouée'
// },
// {
//     author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
//     content: 'sale fontaine de giga tetra foutre que je vais bien manger'
// }])