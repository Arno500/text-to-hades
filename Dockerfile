FROM node:lts

RUN mkdir /app
WORKDIR /app

RUN npm install -g pnpm 
RUN apt-get update \
  && apt-get install -y wget gnupg \
  && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | gpg --dearmor -o /usr/share/keyrings/googlechrome-linux-keyring.gpg \
  && sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/googlechrome-linux-keyring.gpg] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
  && apt-get update \
  && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-khmeros fonts-kacst fonts-freefont-ttf libxss1 ffmpeg \
  --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*

RUN addgroup remotion && useradd -g remotion remotion && chown -R remotion:remotion .

USER remotion

COPY package.json /app
COPY pnpm-lock.yaml /app
RUN pnpm install --frozen-lockfile

COPY . /app/

RUN pnpm run build:all


CMD ["pnpm", "start:prod"]