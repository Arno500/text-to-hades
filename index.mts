import "dotenv/config";
import { ChannelType, Client, Collection, GatewayIntentBits, Guild, Message, TextChannel } from "discord.js";
import { init, render } from "./renderer.mjs";

interface HadesMessage {
  author: {
    id: string,
    name: string,
  },
  content: string,
}

const client = new Client({
  intents: [GatewayIntentBits.Guilds, GatewayIntentBits.MessageContent, GatewayIntentBits.GuildMessages],
});

client.on("interactionCreate", async interaction => {
  if (!interaction.isChatInputCommand()) return;
  await interaction.deferReply();
  const channelData = interaction.channel as TextChannel;

  if (interaction.commandName === "render") {
    let messages: Collection<string, Message<true>> = new Collection();
    const messagesToFetch = interaction.options.getNumber("messages") ?? 5;
    try {
      messages = await channelData.messages.fetch({ limit: 100, before: interaction.id, cache: false })
    } catch (err) {
      console.error(err)
      return
    }
    const toRender = [...messages
      .filter(message =>
        message.content &&
        !message.system &&
        !message.content.match(/\w*:\/\/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/g))
      .values()]
      .slice(0, messagesToFetch)
      .map(message => ({ content: message.cleanContent || message.content, author: { id: message.author.id, name: message.author.username } }))
      .reverse()

    if (toRender.length === 0) {
      await interaction.editReply("nothing to say :(\nPLEASE TALK IN THE CHANNEL");
      return;
    };
    try {
      const videoFile = await render(toRender);

      await interaction.followUp({ files: [videoFile] })
    } catch (err) {
      console.error(err)
      await interaction.editReply("something went wrong :(")
    }
  }
});

client.on("ready", () => {
  if (client.user?.tag) {
    console.log(`Logged in as ${client.user.tag}!`);
  }
});

client.login(process.env.DISCORD_TOKEN);
