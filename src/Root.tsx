import { getAudioDurationInSeconds } from '@remotion/media-utils';
import { useEffect, useState } from 'react';
import { Composition, continueRender, delayRender, getInputProps, staticFile } from 'remotion';
import { Compositor } from './Compositor';

const FPS = 30
const TRANSITION_LENGTH = 30

export const RemotionRoot: React.FC = () => {
	const [lengths, setLengths] = useState<Array<number>>([]);
	const props = {
		messages:
			[
				{
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				}, {
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				}, {
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				}, {
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				}, {
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				}, {
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				}, {
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				}, {
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				}, {
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				}, {
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				}, {
					author: { id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' },
					content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget lacus et tortor lobortis luctus. Quisque placerat sem sed purus aliquet convallis. Etiam tincidunt, purus eu vulputate vestibulum, velit ante.',
					audioSrc: 'voices/0bae31632240a672f798e07582c8a50f.wav'
				},
			],
		// [{
		// 	content: 'Coucou, tu veux voir ma bite ?',
		// 	author: {
		// 		id: 1,
		// 		name: "ArDu"
		// 	},
		// 	audioSrc: 'voices/05520b6cdac9fb6dcb3b38293728f441.mp3',
		// }, {
		// 	content: 'Oui j\'en ai tellement envie',
		// 	author: {
		// 		id: 2,
		// 		name: "ArDu 2"
		// 	},
		// 	audioSrc: 'voices/05520b6cdac9fb6dcb3b38293728f441.mp3',
		// }],
		authors: [{ id: '285821956337696779', name: '5th (☭ ͜ʖ ☭)' }],
		// [{ id: 1, name: "ArDu" }, { id: 2, name: "Kevin" }],
		transitionLength: TRANSITION_LENGTH,
		lengths,
		...getInputProps()
	}
	const [handle] = useState(() => delayRender());
	const [compositionLength, setCompositionLength] = useState(props.messages.length * (TRANSITION_LENGTH * 2));


	useEffect(() => {
		Promise.all(
			props.messages.map((message: any) => getAudioDurationInSeconds(staticFile(message.audioSrc)))
		)
			.then((allDurations) => {
				const durations = allDurations.map(duration => Math.round(duration * FPS) + TRANSITION_LENGTH * 2)
				setLengths(durations)
				setCompositionLength(durations.reduce((acc, val) => acc + val, 0) - (TRANSITION_LENGTH / 2 * (durations.length - 1)));
				continueRender(handle);
			})
			.catch((err) => {
				console.log(`Error fetching metadata: ${err}`);
			});
	}, [handle]);

	if (lengths.length === 0) return <></>
	return (
		<>
			<Composition
				id={"Compositor"}
				component={Compositor}
				fps={FPS}
				width={1920}
				height={1080}
				durationInFrames={compositionLength}
				defaultProps={props}
			/>
		</>
	);
};
