import { createWriteStream, existsSync } from 'fs';
import { open, writeFile, rm } from 'fs/promises';
import md5 from 'md5'
import { got } from "got"
import { pipeline } from 'stream/promises';

type Voice = {
  language: string,
  voice: string,
  noiseScale: number,
  noiseW: number,
  lengthScale: number
}

const allVoices: Array<Voice> = [
  {
    language: "fr_FR",
    voice: "fr_FR/m-ailabs_low#ezwa",
    noiseScale: 0.9,
    noiseW: 0.3,
    lengthScale: 1
  },
  {
    language: "fr_FR",
    voice: "fr_FR/m-ailabs_low#nadine_eckert_boulet",
    noiseScale: 0.6,
    noiseW: 0.3,
    lengthScale: 0.9
  },
  {
    language: "fr_FR",
    voice: "fr_FR/m-ailabs_low#bernard",
    noiseScale: 0.6,
    noiseW: 0.3,
    lengthScale: 0.9
  },
  {
    language: "fr_FR",
    voice: "fr_FR/m-ailabs_low#zeckou",
    noiseScale: 0.1,
    noiseW: 0.6,
    lengthScale: 0.8,
  },
  {
    language: "fr_FR",
    voice: "fr_FR/m-ailabs_low#gilles_g_le_blanc",
    noiseScale: 0.6,
    noiseW: 0.4,
    lengthScale: 0.9,
  },
  {
    language: "fr_FR",
    voice: "fr_FR/siwis_low",
    noiseScale: 0.2,
    noiseW: 0.4,
    lengthScale: 0.9,
  },
  {
    language: "fr_FR",
    voice: "fr_FR/tom_low",
    noiseScale: 0.1,
    noiseW: 0.5,
    lengthScale: 0.7
  }
]

export const textToSpeech = async (
  text: string,
  language: string,
  voiceId: number
): Promise<string> => {
  const languageVoices = allVoices
    .filter((voice: Voice) => voice.language === language)
  const choosenVoice = languageVoices[voiceId % (languageVoices.length - 1)]
  const fileName = `voices/${md5(text + language + choosenVoice.voice)}.wav`;
  const fileExists = await checkIfAudioHasAlreadyBeenSynthesized("public/" + fileName);

  if (fileExists) {
    return fileName;
  }

  try {
    const fileWriterStream = createWriteStream("public/" + fileName);
    const result = got.stream(process.env.MIMIC_URL + "/api/tts" ?? "", {
      searchParams: {
        ...choosenVoice,
        ssml: "false",
        audioTarget: "client",
        text
      },
      timeout: {
        response: 30000
      },
      retry: {
        limit: 10,
        backoffLimit: 10000,
        methods: ["GET"]
      },
    })

    result.on('error', (err) => {
      console.error(err)
      rm("public/" + fileName)
    })
    fileWriterStream.on('error', (err) => {
      console.error(err)
      rm("public/" + fileName)
    })

    await pipeline(result, fileWriterStream)

    const file = await open("public/" + fileName, 'r+')
    await file.datasync()
    file.close()

    return fileName;
  } catch (err) {
    console.error(err)
    return ""
  }

};

const checkIfAudioHasAlreadyBeenSynthesized = async (fileName: string) => existsSync(fileName)
