import { ApplicationCommandData, ApplicationCommandOptionType, REST, Routes } from "discord.js";
import "dotenv/config";

const commands: Array<ApplicationCommandData> = [
  {
    name: "render",
    description: "Text To Hades",
    options: [{
      name: "messages",
      description: "The number of messages to include. By default it is 5 and cannot be over 20.",
      type: ApplicationCommandOptionType.Number,
      maxValue: 20,
      autocomplete: false,
      required: false,
    }]
  }
];

const rest = new REST({ version: "10" }).setToken(process.env.DISCORD_TOKEN ?? "ratio token");

(async () => {
  try {
    console.log("Started refreshing application (/) commands.");

    await rest.put(Routes.applicationCommands(process.env.DISCORD_CLIENT_ID ?? "ratio client id"), { body: commands });

    console.log("Successfully reloaded application (/) commands.");
  } catch (error) {
    console.error(error);
  }
})();
