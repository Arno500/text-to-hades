import { CameraMotionBlur } from '@remotion/motion-blur';
import { useRef, useState } from 'react';
import { AbsoluteFill, Img, random, RandomSeed, Sequence, staticFile } from 'remotion';
import { Title } from './Compositor/Title';

type Message = {
	content: string;
	author: {
		id: string;
		name: string;
	};
	audioSrc: string;
}

type Messages = Array<Message>

const characters = [
	"Achilles.webp",
	"Alecto.webp",
	"Aphrodite.webp",
	"Ares.webp",
	"Artemis.webp",
	"Asterius.webp",
	"Athena.webp",
	"Bouldy.webp",
	"Cerberus.webp",
	"Chaos.webp",
	"Charon.webp",
	"Demeter.webp",
	"Dionysus.webp",
	"EM_Asterius.webp",
	"Eurydice.webp",
	"Hades.webp",
	"Hypnos_newest.webp",
	"Magaera.webp",
	"Nyx.webp",
	"Orpheus_new.webp",
	"Patroclus.webp",
	"Persephone_Queen.webp",
	"Sisyphus_final.webp",
	"Sisyphus.webp",
	"Thanatos.webp",
	"Theseus.webp",
	"Tisiphone.webp",
	"Zagreus.webp",
	"Zeus.webp",
]

const backgrounds = [
	"assets/background.png",
	"assets/background2.png",
	"assets/background3.png",
]

function shuffle(sourceArray: Array<any>, seed: RandomSeed) {
	const arrayCopy = [...sourceArray]
	for (var i = 0; i < arrayCopy.length - 1; i++) {
		var j = i + Math.floor(random(seed) * (arrayCopy.length - i));

		var temp = arrayCopy[j];
		arrayCopy[j] = arrayCopy[i];
		arrayCopy[i] = temp;
	}
	return arrayCopy;
}

export const Compositor: React.FC<{
	messages: Messages,
	transitionLength: number,
	lengths: Array<number>,
	authors: Array<{ id: string, name: string }>
}> = ({ messages, transitionLength, lengths, authors }) => {
	const shuffledCharacters = useRef(shuffle(characters, messages.reduce((acc, message) => acc + message.author.id, '')))

	let frameCount = 0
	const sequences: Array<{
		duration: number,
		offset: number,
		character: string,
		message: Message
	}> = []

	messages.forEach((message: Message, index) => {
		sequences.push({
			duration: lengths[index] || (transitionLength * 3),
			offset: frameCount,
			message,
			character: shuffledCharacters.current[Math.floor(random(message.author.id) * shuffledCharacters.current.length)]
		})
		frameCount += lengths[index] - transitionLength / 2 || (transitionLength * 3)
	})

	const background = backgrounds[Math.floor(random(JSON.stringify(authors)) * backgrounds.length)]
	return (
		// <CameraMotionBlur shutterAngle={180} samples={5}>
		<div style={{ flex: 1, backgroundColor: 'black' }}>
			<AbsoluteFill>
				<Img src={staticFile(background)} />
			</AbsoluteFill>
			{sequences.map((sequence, index) =>
				<Sequence key={index} from={index === 0 ? 0 : Math.round(sequence.offset)} durationInFrames={Math.round(sequence.duration)}>
					<Title key={index} message={sequence.message} authors={authors} transitionLength={transitionLength} duration={Math.round(sequence.duration)} character={sequence.character} />
				</Sequence>)}
		</div>
		// </CameraMotionBlur>
	);
};
