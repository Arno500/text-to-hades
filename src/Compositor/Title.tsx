// @ts-ignore
import { loadFont as CaesarDressing } from "@remotion/google-fonts/CaesarDressing";
// @ts-ignore
import { loadFont as AlegreyaSans } from "@remotion/google-fonts/AlegreyaSans";
import {
	AbsoluteFill,
	Audio,
	Easing,
	Img,
	interpolate,
	random,
	Sequence,
	staticFile,
	useCurrentFrame,
} from 'remotion';

const { fontFamily: caesarDressingFF } = CaesarDressing("normal", {
	subsets: ["latin"],
})
const { fontFamily: alegreyaSansFF } = AlegreyaSans("normal", {
	subsets: ["latin", "latin-ext"]
})

const characterOffset = 3

export const Title: React.FC<{
	message: {
		audioSrc: string,
		author: {
			id: string,
			name: string
		},
		content: string
	},
	transitionLength: number,
	duration: number,
	authors: Array<{ id: string, name: string }>,
	character: string
}> = ({ message, transitionLength, duration, authors, character }) => {
	const frame = useCurrentFrame();
	const audioFile = staticFile(message.audioSrc);

	const windowTranslateAnimation = interpolate(frame, [0, transitionLength, duration - transitionLength + 1, duration - 1], [20, 0, 0, -20], {
		extrapolateLeft: 'clamp',
		extrapolateRight: 'clamp',
		easing: Easing.bezier(0, 0.6, 0, 0.8)
	})
	const windowOpacityAnimation = interpolate(frame, [0, transitionLength, duration - transitionLength + 1, duration - 1], [0, 1, 1, 0], {
		extrapolateLeft: 'clamp',
		extrapolateRight: 'clamp',
		easing: Easing.bezier(0.25, 0.65, 0.2, 0.8)
	})
	const characterTranslateAnimation = interpolate(frame, [characterOffset, transitionLength + characterOffset, duration - transitionLength + characterOffset, duration - 1], [20, 0, 0, -20], {
		extrapolateLeft: 'clamp',
		extrapolateRight: 'clamp',
		easing: Easing.bezier(0, 0.6, 0, 0.8)
	})
	const characterOpacityAnimation = interpolate(frame, [characterOffset, transitionLength + characterOffset, duration - transitionLength + characterOffset, duration - 1], [0, 1, 1, 0], {
		extrapolateLeft: 'clamp',
		extrapolateRight: 'clamp',
		easing: Easing.bezier(0.25, 0.65, 0.2, 0.8)
	})
	return (
		<>
			{message.audioSrc ? <Sequence from={transitionLength} name="Audio"><Audio src={audioFile} volume={1} /></Sequence> : <></>}
			<div style={{
				transform: `translateX(${characterTranslateAnimation}px)`,
				opacity: characterOpacityAnimation,
				position: 'absolute',
				bottom: 0,
				left: "-15%",
				width: "60%",
				height: "70%",
				display: "flex",
				justifyContent: "center",
				alignItems: "center"
			}}>
				<Img src={staticFile('assets/characters/' + character)} style={{
					position: 'relative',
					bottom: 0,
					left: 0,
					zIndex: -1,
					width: "100%",
				}} />
			</div>
			<div style={{
				transform: `translateX(${windowTranslateAnimation}px)`,
				opacity: windowOpacityAnimation,
				position: 'absolute',
				bottom: 0,
				left: "30%"
			}}>
				<h2 style={{
					position: "absolute",
					top: 110,
					left: 150,
					color: "#e1dddd",
					fontFamily: caesarDressingFF,
					fontSize: "2.5em",
					margin: 0,
				}}>{message.author.name}</h2>
				<p style={{
					position: "absolute",
					fontFamily: alegreyaSansFF,
					color: "#2b2726",
					fontWeight: "800",
					top: 215,
					left: 113,
					fontSize: "2.5em",
					width: "82%",
					height: "31%",
					textOverflow: "ellipsis",
					overflow: "hidden",
					whiteSpace: "break-spaces",
					display: "-webkit-box",
					WebkitLineClamp: 3,
					WebkitBoxOrient: "vertical"
				}}>{message.content}</p>
				<Img src={staticFile('assets/cadre_texte.png')} style={{
					position: 'relative',
					bottom: 0,
					left: 0,
					zIndex: -1
				}} />
			</div>
		</>
	);
};
